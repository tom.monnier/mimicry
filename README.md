# Mimicry for Masquerade
Mimicry is a cuba plugin that allows to generate java files for Masquerade Tests.

It parses screen descriptor xml files and for each generates a source file containing a class with Masquerade components.

For instance, running Mimicry on the owner package for petclinic sample will generate code for the OwnerBrowse and OwnerEditor screens. (see below)
It also parses the menu structure and generates a file containing menu entries. Note, it only includes menu entries if the corresponding screens are included.

To run, simply instal the addon, run the application and open the Mimicry screen (make sure you have the permissions or login as admin). There you can enter a comma seperated list of java packages.

Oh, and it (should) support fragments.

Mimicry will then create a zip file containing the source.

#Important notes
* This is still a poc, I did not review the code and it will probably be rather dirty.
* Masquerade does not have a component for side menu, for the petclinic sample I had to add one to make the test work. 
* The generated classes have the same packages as the original screens. This is not an issue in my eyes as they are meant to use in a 
different java application outside of the cuba project. But trying to use them in the same application will result in funny/crazy 
behaviour since they have the sane fqn. I might change my mind about the target packages later... 
* Currently it only supports @UIController annotated classes, but it seems adding legacy screens would be trivial.
* I have not been able to test all the components yet.
* Some components are probably still missing. (eg., optionsgroup)
* Dynamically added fields are not supported.
* There seems to be an issue that the classpath is not always scanned recursively running Mimicry on 
com.haulmont.cuba.web.app.login generates the LoginScreen class, but running it on com.haulmont.cuba.web.app doesnt?

#Samples
```java
package com.haulmont.cuba.web.app.login;

import com.haulmont.masquerade.Wire;
import com.haulmont.masquerade.components.Button;
import com.haulmont.masquerade.components.CheckBox;
import com.haulmont.masquerade.components.LookupField;
import com.haulmont.masquerade.components.PasswordField;
import com.haulmont.masquerade.components.TextField;

public final class LoginScreen {
  @Wire(
      path = "loginField"
  )
  public TextField loginField;

  @Wire(
      path = "passwordField"
  )
  public PasswordField passwordField;

  @Wire(
      path = "rememberMeCheckBox"
  )
  public CheckBox rememberMeCheckBox;

  @Wire(
      path = "localesSelect"
  )
  public LookupField localesSelect;

  @Wire(
      path = "loginButton"
  )
  public Button loginButton;
}

```


```java
package be.octorilla.mimicry;
 
 import com.haulmont.sample.petclinic.web.owner.owner.OwnerBrowse;
 import com.haulmont.sample.petclinic.web.pet.pet.PetBrowse;
 import com.haulmont.sample.petclinic.web.screens.pet.pettype.PetTypeBrowse;
 import com.haulmont.sample.petclinic.web.screens.veterinarian.specialty.SpecialtyBrowse;
 import com.haulmont.sample.petclinic.web.screens.veterinarian.veterinarian.VeterinarianBrowse;
 import com.haulmont.sample.petclinic.web.screens.visit.VisitBrowse;
 
 public final class AppMenu {
   public static com.haulmont.masquerade.components.AppMenu.Menu<VisitBrowse> petclinic_Visit_browse = new com.haulmont.masquerade.components.AppMenu.Menu(VisitBrowse.class, "application-petclinic","petclinic_Visit.browse");
   ;
 
   public static com.haulmont.masquerade.components.AppMenu.Menu<PetBrowse> petclinic_Pet_browse = new com.haulmont.masquerade.components.AppMenu.Menu(PetBrowse.class, "application-petclinic","petclinic_Pet.browse");
   ;
 
   public static com.haulmont.masquerade.components.AppMenu.Menu<OwnerBrowse> petclinic_Owner_browse = new com.haulmont.masquerade.components.AppMenu.Menu(OwnerBrowse.class, "application-petclinic","petclinic_Owner.browse");
   ;
 
   public static com.haulmont.masquerade.components.AppMenu.Menu<VeterinarianBrowse> petclinic_Veterinarian_browse = new com.haulmont.masquerade.components.AppMenu.Menu(VeterinarianBrowse.class, "application-masterdata","petclinic_Veterinarian.browse");
   ;
 
   public static com.haulmont.masquerade.components.AppMenu.Menu<PetTypeBrowse> petclinic_PetType_browse = new com.haulmont.masquerade.components.AppMenu.Menu(PetTypeBrowse.class, "application-masterdata","petclinic_PetType.browse");
   ;
 
   public static com.haulmont.masquerade.components.AppMenu.Menu<SpecialtyBrowse> petclinic_Specialty_browse = new com.haulmont.masquerade.components.AppMenu.Menu(SpecialtyBrowse.class, "application-masterdata","petclinic_Specialty.browse");
   ;
 }

```



```java
package com.haulmont.sample.petclinic.web.owner.owner;

import com.haulmont.masquerade.Wire;
import com.haulmont.masquerade.components.Button;

public final class OwnerBrowse {
  @Wire(
      path = "createBtn"
  )
  public Button createBtn;

  @Wire(
      path = "viewBtn"
  )
  public Button viewBtn;

  @Wire(
      path = "excelBtn"
  )
  public Button excelBtn;

  @Wire(
      path = "removeBtn"
  )
  public Button removeBtn;

  @Wire(
      path = "lookupSelectAction"
  )
  public Button lookupSelectAction;

  @Wire(
      path = "lookupCancelAction"
  )
  public Button lookupCancelAction;
}

```

```java
package com.haulmont.sample.petclinic.web.owner.owner;

import com.haulmont.masquerade.Wire;
import com.haulmont.masquerade.components.Button;
import com.haulmont.masquerade.components.Table;
import com.haulmont.masquerade.components.TextField;

public final class OwnerEdit {
  @Wire(
      path = "firstName"
  )
  public TextField firstName;

  @Wire(
      path = "address"
  )
  public TextField address;

  @Wire(
      path = "telephone"
  )
  public TextField telephone;

  @Wire(
      path = "lastName"
  )
  public TextField lastName;

  @Wire(
      path = "city"
  )
  public TextField city;

  @Wire(
      path = "email"
  )
  public TextField email;

  @Wire(
      path = "petsTable"
  )
  public Table petsTable;

  @Wire(
      path = "petsTable_create"
  )
  public Button petsTable_create;

  @Wire(
      path = "petsTable_edit"
  )
  public Button petsTable_edit;

  @Wire(
      path = "petsTable_remove"
  )
  public Button petsTable_remove;

  @Wire(
      path = "windowCommitAndClose"
  )
  public Button windowCommitAndClose;

  @Wire(
      path = "windowClose"
  )
  public Button windowClose;

  @Wire(
      path = "enableEditing"
  )
  public Button enableEditing;
}

```