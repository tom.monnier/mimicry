package be.octorilla.mimicry.web.screens.util;

public class UnnamedComponentCounter {
    static int value = 1;

    public static int next() {
        return value++;
    }
}
