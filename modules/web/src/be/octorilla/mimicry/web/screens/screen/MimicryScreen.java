package be.octorilla.mimicry.web.screens.screen;

import com.haulmont.cuba.gui.config.WindowInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
public class MimicryScreen {
    private Class screenClass;
    private ScreenStructure screenStructure;
    private WindowInfo windowInfo;
    @Setter
    private String javaFile;

    public MimicryScreen(Class screenClass, ScreenStructure screenStructure, WindowInfo windowInfo) {
        this.screenClass = screenClass;
        this.screenStructure = screenStructure;
        this.windowInfo = windowInfo;
    }
}



