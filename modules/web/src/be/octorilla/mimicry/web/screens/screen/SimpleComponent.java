package be.octorilla.mimicry.web.screens.screen;

import be.octorilla.mimicry.web.screens.util.UnnamedComponentCounter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class SimpleComponent {
    private String id;
    private String property;
    private String action;
    private UiElementType type;

    public String getName() {
        if (id != null) {
            return id;
        } else if (property != null) {
            return property;
        } else if (action != null) {
            return action.replace('.', '_');
        } else {
            return type.getXmlName() + "_" + UnnamedComponentCounter.next();
        }
    }

}

