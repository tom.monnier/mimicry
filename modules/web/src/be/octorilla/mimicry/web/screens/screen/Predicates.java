package be.octorilla.mimicry.web.screens.screen;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Predicates {
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
