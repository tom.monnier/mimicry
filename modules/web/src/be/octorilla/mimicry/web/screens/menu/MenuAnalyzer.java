package be.octorilla.mimicry.web.screens.menu;

import com.haulmont.cuba.core.global.Resources;
import com.haulmont.cuba.core.sys.AppContext;
import com.haulmont.cuba.core.sys.xmlparsing.Dom4jTools;
import com.squareup.javapoet.ClassName;
import org.apache.commons.text.StringTokenizer;
import org.dom4j.Element;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

import static java.util.Optional.ofNullable;

@Component
public class MenuAnalyzer {
    @Inject
    protected Resources resources;
    @Inject
    protected Dom4jTools dom4JTools;

    public static final String MENU_CONFIG_XML_PROP = "cuba.menuConfig";

    public MimicryMenu loadmenuItems(Map<String, ClassName> screenMap) {
        String configName = AppContext.getProperty(MENU_CONFIG_XML_PROP);
        MimicryMenu mimicryMenu = new MimicryMenu(null);

        StringTokenizer tokenizer = new StringTokenizer(configName);
        for (String location : tokenizer.getTokenArray()) {
            Resource resource = resources.getResource(location);
            if (resource.exists()) {
                try (InputStream stream = resource.getInputStream()) {
                    Element rootElement = dom4JTools.readDocument(stream).getRootElement();
                    loadMenuItems(rootElement.elements(), new Stack<>(), mimicryMenu, screenMap);
                } catch (IOException e) {
                    throw new RuntimeException("Unable to read menu config", e);
                }
            }
        }
        return mimicryMenu;
    }

    private void loadMenuItems(List<Element> elements, Stack<String> parents, MimicryMenu menu, Map<String, ClassName> screenMap) {
        for (Element element : elements) {
            String id = element.attributeValue("id");
            String screen = element.attributeValue("screen");

            if ("item".equals(element.getName()) && screen != null && screenMap.containsKey(screen)) {
                parents.push(Optional.ofNullable(id).orElse(screen));
                menu.getScreens().add(new MimicryMenuItem(screenMap.get(screen), ofNullable(id).orElse(screen), parents.toArray(new String[0])));
                parents.pop();
            } else if ("menu".equals(element.getName()) && id != null) {
                MimicryMenu submenu = new MimicryMenu(id);
                menu.getSubMenus().add(submenu);
                parents.push(id);
                loadMenuItems(element.elements(), parents, submenu, screenMap);
                parents.pop();
            }
        }
    }
}
