package be.octorilla.mimicry.web.screens.menu;

import be.octorilla.mimicry.web.screens.util.HasId;
import com.google.common.base.Joiner;
import com.squareup.javapoet.ClassName;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.util.Arrays.stream;

@Getter
@AllArgsConstructor
public class MimicryMenuItem implements HasId {
    private ClassName className;
    private String id;
    private String[] path;


    public String getPathAsStringVar() {
        String[] quoted = stream(path).map(i -> "\"" + i + "\"").toArray(i -> new String[i]);
        return Joiner.on(',').join(quoted);
    }
}
