package be.octorilla.mimicry.web.screens.export;

import be.octorilla.mimicry.web.screens.application.MimicryApplication;
import be.octorilla.mimicry.web.screens.menu.MimicryMenuItem;
import be.octorilla.mimicry.web.screens.screen.FragmentComponent;
import be.octorilla.mimicry.web.screens.screen.MimicryScreen;
import be.octorilla.mimicry.web.screens.screen.SimpleComponent;
import com.haulmont.masquerade.Wire;
import com.haulmont.masquerade.components.AppMenu;
import com.squareup.javapoet.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.stream.Collectors;

import static javax.lang.model.element.Modifier.*;

@Component
public class ClassGenerator {
    public static String MENU_PACKAGE = "be.octorilla.mimicry";
    public static String MENUCLASSNAME = "AppMenu";
    public static String MENUFQN = MENU_PACKAGE + "." + MENUCLASSNAME;


    public void writeClass(MimicryScreen screen, Map<String, ClassName> fragmentMap) {
        TypeSpec typespec = TypeSpec
                .classBuilder(screen.getScreenClass().getSimpleName())
                .addModifiers(PUBLIC, FINAL)
                .addFields(screen.getScreenStructure().getSimpleComponents().stream().map(this::toMasqueradeType).collect(Collectors.toList()))
                .addFields(screen.getScreenStructure().getFragmentComponents().stream().filter(c -> fragmentMap.containsKey(c.getFramgent())).map(f -> toMasqueradeType(f, fragmentMap)).collect(Collectors.toList()))
                .build();
        screen.setJavaFile(writeToString(screen.getScreenClass().getPackage().getName(), typespec));
    }

    private String writeToString(String packageName, TypeSpec typeSpec) {
        JavaFile javaFile = JavaFile.builder(packageName, typeSpec).build();
        StringWriter writer = new StringWriter();
        try {
            javaFile.writeTo(writer);
            return writer.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeMenuStruture(MimicryApplication application) {
        TypeSpec typespec = TypeSpec
                .classBuilder(MENUCLASSNAME)
                .addModifiers(PUBLIC, FINAL)
                .addFields(application.getMenu().flatten().stream().map(this::toMenuItem).collect(Collectors.toList()))
                .build();
        application.setJavaFile(writeToString(MENU_PACKAGE, typespec));
    }

    private FieldSpec toMenuItem(MimicryMenuItem item) {
        FieldSpec.Builder builder = FieldSpec.builder(ParameterizedTypeName.get(ClassName.get(AppMenu.Menu.class), item.getClassName()), item.getJavaSafeId(), PUBLIC, STATIC)
                .initializer(CodeBlock.builder().addStatement("new $T($T.class, $L)", AppMenu.Menu.class, item.getClassName(), item.getPathAsStringVar()).build());
        return builder.build();
    }

    public FieldSpec toMasqueradeType(SimpleComponent simpleComponent) {
        FieldSpec.Builder builder = FieldSpec.builder(simpleComponent.getType().getTargetType(), simpleComponent.getName(), PUBLIC);
        if (simpleComponent.getName() != null) {
            builder.addAnnotation(AnnotationSpec.builder(Wire.class).addMember("path", CodeBlock.builder().add("$S", simpleComponent.getName()).build()).build());
        }
        return builder.build();
    }

    protected FieldSpec toMasqueradeType(FragmentComponent fragmentComponent, Map<String, ClassName> fragmentMap) {
        FieldSpec.Builder builder = FieldSpec.builder(fragmentMap.get(fragmentComponent.getFramgent()), fragmentComponent.getName(), PUBLIC);
        return builder.build();
    }
}
