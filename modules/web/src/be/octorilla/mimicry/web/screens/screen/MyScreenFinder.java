package be.octorilla.mimicry.web.screens.screen;

import com.haulmont.cuba.gui.config.WindowConfig;
import com.haulmont.cuba.gui.config.WindowInfo;
import com.haulmont.cuba.gui.sys.UiControllersConfiguration;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MyScreenFinder extends UiControllersConfiguration {
    @Inject
    private WindowConfig windowConfig;

    public Collection<WindowInfo> getWindowInfos(List<String> packages) {
        return windowConfig.getWindows().stream().filter(w -> isInIncludedPackages(w, packages)).collect(Collectors.toList());
    }

    private boolean isInIncludedPackages(WindowInfo info, List<String> packages) {
        return packages.stream().filter(p -> info.getControllerClass().getCanonicalName().startsWith(p)).findAny().isPresent();
    }

}
