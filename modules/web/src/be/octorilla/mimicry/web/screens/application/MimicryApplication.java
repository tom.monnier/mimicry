package be.octorilla.mimicry.web.screens.application;

import be.octorilla.mimicry.web.screens.menu.MimicryMenu;
import be.octorilla.mimicry.web.screens.screen.MimicryScreen;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

@Getter
public class MimicryApplication {
    private MimicryMenu menu;
    private List<MimicryScreen> screens;
    @Setter
    private String javaFile;

    public MimicryApplication(MimicryMenu menu, List<MimicryScreen> screens) {
        this.menu = menu;
        this.screens = screens;
    }

    public Optional<Class> getScreenClass(String screenId) {
        return screens.stream().filter(s -> s.getWindowInfo().getId().equals(screenId)).map(MimicryScreen::getScreenClass).findFirst();
    }
}
