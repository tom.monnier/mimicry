package be.octorilla.mimicry.web.screens.menu;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class MimicryMenu {
    private String id;
    private List<MimicryMenuItem> screens = new ArrayList<>();
    private List<MimicryMenu> subMenus = new ArrayList<>();

    public MimicryMenu(String id) {
        this.id = id;
    }

    public List<MimicryMenuItem> flatten() {
        List<MimicryMenuItem> result = new ArrayList<>();
        result.addAll(screens);
        for (MimicryMenu subMenu : subMenus) {
            result.addAll(subMenu.flatten());
        }
        return result;
    }
}
