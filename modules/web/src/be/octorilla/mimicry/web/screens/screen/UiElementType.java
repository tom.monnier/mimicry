package be.octorilla.mimicry.web.screens.screen;

import com.haulmont.masquerade.base.SelenideElementWrapper;
import com.haulmont.masquerade.components.*;
import lombok.Getter;

import static java.util.Arrays.stream;

@Getter
public enum UiElementType {
    BUTTON("button", Button.class),
    CHECKBOX("checkBox", CheckBox.class),
    //not supported by masquerade
    //CHECKBOXGROUP("checkBoxGroup", CheckBoxgr.class),
    DATAGRID("dataGrid", DataGrid.class),
    DATEFIELD("dateField", DateField.class),
    DATETIMEFIELD("dateTimeField", DateTimeField.class),
    //not supported by maquerade
    //DATEPICKER("datePicker", Datepicker),
    GROUPTABLE("groupTable", GroupTable.class),
    LOOKUPFIELD("lookupField", LookupField.class),
    LOOKUPPICKERFIELD("lookupPickerField", LookupPickerField.class),
    MASKEDFIEKLD("maskedField", MaskedField.class),
    PASSWORDFIELD("passwordField", PasswordField.class),
    OPTIONSGROUP("optionsGroup", OptionsGroup.class),
    PICKERFIELD("pickerField", PickerField.class),
    TABLE("table", Table.class),
    //Masquerade should have Tab implement Component
    TAB("tab", TabSheet.Tab.class),
    TABSHEET("tabSheet", TabSheet.class),
    TEXTFIELD("textField", TextField.class),
    TEXTAREA("textArea", TextArea.class),
    TIMEFIELD("timeField", TimeField.class)
    //Not supported by masquerade?
    //TREETABLE("treeTable", treetable)  ;
    ;

    private final String xmlName;
    private Class<? extends SelenideElementWrapper> targetType;

    // should be
    // Class<? extends Component >
    UiElementType(String xmlName, Class<? extends SelenideElementWrapper> clazz) {
        this.xmlName = xmlName;
        targetType = clazz;
    }

    public static UiElementType fromXml(String xmlName) {
        return stream(UiElementType.values()).filter(u -> u.xmlName.equals(xmlName)).findFirst().orElseThrow(() -> new RuntimeException("unsupported UI type"));
    }

    public static boolean isSimpleComponent(String xmlName) {
        return stream(UiElementType.values()).filter(u -> u.xmlName.equals(xmlName)).findFirst().isPresent();
    }
}
