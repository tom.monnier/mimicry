package be.octorilla.mimicry.web.screens.screen;

import org.dom4j.Element;
import org.dom4j.VisitorSupport;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ScreenXmlAnalyzer {
    public ScreenStructure analyze(Element descriptor) {
        List<SimpleComponent> simpleComponents = new ArrayList<>();
        List<FragmentComponent> fragmentComponents = new ArrayList<>();

        descriptor.accept(new VisitorSupport() {
            public void visit(Element element) {
                if (UiElementType.isSimpleComponent(element.getQualifiedName())) {
                    simpleComponents.add(toSimpleComponent(element));

                } else if ("fragment".equals(element.getQualifiedName())) {
                    fragmentComponents.add(new FragmentComponent(element.attributeValue("id"), element.attributeValue("screen")));
                }
            }
        });
        return new ScreenStructure(simpleComponents, fragmentComponents);
    }


    protected SimpleComponent toSimpleComponent(Element e) {
        return new SimpleComponent(e.attributeValue("id"),e.attributeValue("property"), e.attributeValue("action"), UiElementType.fromXml(e.getName()));
    }
}
