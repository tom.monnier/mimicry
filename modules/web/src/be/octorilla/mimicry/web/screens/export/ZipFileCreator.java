package be.octorilla.mimicry.web.screens.export;

import be.octorilla.mimicry.web.screens.application.MimicryApplication;
import be.octorilla.mimicry.web.screens.screen.MimicryScreen;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
public class ZipFileCreator {
    public byte[] createZip(MimicryApplication application) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(out);

            for (MimicryScreen screen : application.getScreens()) {
                writeEntry(zipOutputStream, toFileName(screen.getScreenClass().getName()), screen.getJavaFile());
            }
            writeEntry(zipOutputStream,toFileName(ClassGenerator.MENUFQN), application.getJavaFile());
            zipOutputStream.close();
            return out.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeEntry(ZipOutputStream zipOutputStream, String fileName, String content)  throws IOException {
        ZipEntry entry = new ZipEntry(fileName);
        zipOutputStream.putNextEntry(entry);
        zipOutputStream.write(content.getBytes());
    }

    private String toFileName(String fqn) {
        return fqn.replace('.', '/') + ".java";
    }
}
