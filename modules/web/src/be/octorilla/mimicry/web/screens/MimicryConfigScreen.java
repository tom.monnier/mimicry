package be.octorilla.mimicry.web.screens;

import be.octorilla.mimicry.web.screens.application.MimicryApplication;
import be.octorilla.mimicry.web.screens.export.ClassGenerator;
import be.octorilla.mimicry.web.screens.export.ZipFileCreator;
import be.octorilla.mimicry.web.screens.menu.MenuAnalyzer;
import be.octorilla.mimicry.web.screens.screen.MimicryScreen;
import be.octorilla.mimicry.web.screens.screen.MyScreenFinder;
import be.octorilla.mimicry.web.screens.screen.Predicates;
import be.octorilla.mimicry.web.screens.screen.ScreenXmlAnalyzer;
import com.google.common.base.Splitter;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.config.WindowConfig;
import com.haulmont.cuba.gui.config.WindowInfo;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.screen.*;
import com.haulmont.cuba.gui.sys.UiControllerDefinition;
import com.haulmont.cuba.gui.sys.UiDescriptorUtils;
import com.haulmont.cuba.gui.xml.layout.ScreenXmlLoader;
import com.squareup.javapoet.ClassName;
import org.dom4j.Element;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@UiController("mimicry_configScreen")
@UiDescriptor("MimicryConfigScreen.xml")
public class MimicryConfigScreen extends Screen {
    @Inject
    private TextField<String> packagesField;
    @Inject
    private ExportDisplay exportDisplay;
    @Inject
    private ClassGenerator classWriter;
    @Inject
    private ScreenXmlAnalyzer analyzer;
    @Inject
    private MyScreenFinder screenFinder;
    @Inject
    private WindowConfig windowConfig;
    @Inject
    private ScreenXmlLoader screenXmlLoader;
    @Inject
    private ZipFileCreator zipper;
    @Inject
    private MenuAnalyzer analyuzer;

    private String basePackage = "com.haulmont.cuba.gui.app, com.haulmont.cuba.web.app";

    @Subscribe
    public void beforeShow(BeforeShowEvent e) {
        packagesField.setValue(basePackage);
    }


    public void collectScreenMetadata() {
        String packages = packagesField.getValue();

        List<MimicryScreen> screens = screenFinder.getWindowInfos(Splitter.on(',').splitToList(packages))
                .stream()
                .filter(d -> windowConfig.getWindowInfo(d.getId()).getTemplate() != null)
                .filter(Predicates.distinctByKey(w -> w.getControllerClass()))
                .map(this::dumpScreenMetaData).collect(Collectors.toList());


        MimicryApplication application = new MimicryApplication(analyuzer.loadmenuItems(buildScreenMap(screens)), screens);

        screens.forEach(s -> classWriter.writeClass(s, buildFragmentMap(screens)));
        classWriter.writeMenuStruture(application);

        exportDisplay.show(new ByteArrayDataProvider(zipper.createZip(application)), "test.zip");
    }

    @Subscribe("exportScreenMetadata")
    public void onExportScreenMetadata(Action.ActionPerformedEvent event) {
        collectScreenMetadata();
    }

    public MimicryScreen dumpScreenMetaData(WindowInfo info) {
        String templatePath = info.getTemplate();
        Element descriptor = screenXmlLoader.load(templatePath, info.getId(), Collections.emptyMap());
        return new MimicryScreen(info.getControllerClass(), analyzer.analyze(descriptor), info);
    }

    public MimicryScreen dumpScreenMetaData(UiControllerDefinition definition) {
        try {
            Class<? extends FrameOwner> controllerClass = (Class<? extends FrameOwner>) Class.forName(definition.getControllerClass());
            UiController uiController = controllerClass.getAnnotation(UiController.class);
            UiDescriptorUtils.getInferredScreenId(uiController, controllerClass);
            WindowInfo info = windowConfig.getWindowInfo(definition.getId());

            String templatePath = info.getTemplate();
            Element descriptor = screenXmlLoader.load(templatePath, info.getId(), Collections.emptyMap());
            return new MimicryScreen(controllerClass, analyzer.analyze(descriptor), info);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, ClassName> buildScreenMap(List<MimicryScreen> screens) {
        return screens.stream().collect(Collectors.toMap(s -> s.getWindowInfo().getId(), this::fromFragment));
    }

    public Map<String, ClassName> buildFragmentMap(List<MimicryScreen> screens) {
        return screens.stream()
                .filter(s -> ScreenFragment.class.isAssignableFrom(s.getScreenClass()))
                .collect(Collectors.toMap(s -> s.getWindowInfo().getId(), this::fromFragment));
    }

    private ClassName fromFragment(MimicryScreen screen) {
        return ClassName.get(screen.getWindowInfo().getControllerClass());
    }
}