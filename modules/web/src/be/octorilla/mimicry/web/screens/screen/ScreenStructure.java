package be.octorilla.mimicry.web.screens.screen;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ScreenStructure {
    private List<SimpleComponent> simpleComponents;
    private List<FragmentComponent> fragmentComponents;
}
