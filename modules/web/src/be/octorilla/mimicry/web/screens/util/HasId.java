package be.octorilla.mimicry.web.screens.util;

public interface HasId {
    String getId();

    default String getJavaSafeId() {
        return getId()
                .replace('.', '_')
                .replace('-', '_');
    }
}
