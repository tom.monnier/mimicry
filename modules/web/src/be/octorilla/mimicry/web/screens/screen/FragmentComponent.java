package be.octorilla.mimicry.web.screens.screen;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

@AllArgsConstructor
@Getter
public class FragmentComponent {
    private String id;
    private String framgent;

    public String getName() {
        return Optional.ofNullable(id).orElse(framgent);
    }
}
